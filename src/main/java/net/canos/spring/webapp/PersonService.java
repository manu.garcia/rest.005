package net.canos.spring.webapp;

import org.springframework.stereotype.Service;

@Service
public class PersonService {
	public PersonDTO findById(Integer id){
		if(id <= 0) return null;
		Person p = new Person();
		return new PersonDTO(p);
	}
	public PersonDTO save(PersonForm p){
		return new PersonDTO(p);
	}
	public PersonDTO create(PersonForm p){
		return new PersonDTO(p);
	}
	public void delete(Integer id){
		
	}
}
